import frame_pack.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Test_frame
{
    @Test
    public void bagetSquareTest()
    {
        Baget baget = new Baget((float)0.3 ,(float)0.4,(float)8.3);
        assertEquals(baget.calculateSquare(),2*(baget.getHeight() + baget.getWidth()),0.001);
    }

    @Test
    public void bagetPriceTest()
    {
        Baget baget = new Baget((float)0.3 ,(float)0.4,(float)8.3);
        assertEquals(baget.calculatePrice(),2*(baget.getWidth() + baget.getHeight())*baget.getPerOnePrice(),0.001);
    }
    ////////////////////////////////////////////////////////
    @Test
    public void glassSquareTest()
    {
        Glass glass = new Glass((float)0.3 ,(float)0.4,(float)5.4);
        assertEquals(glass.calculateSquare(),glass.getHeight()*glass.getWidth(),0.001);

    }

    @Test
    public void glassPriceTest()
    {
        Glass glass = new Glass((float)0.3 ,(float)0.4,(float)5.4);
        assertEquals(glass.calculatePrice(),glass.getHeight()* glass.getWidth()* glass.getPerOnePrice(),0.001);

    }

    ////////////////////////////////////////////////////////
    @Test
    public void faneraSquareTest()
    {
        Fanera fanera = new Fanera((float)0.3 ,(float)0.4,(float)3.3);
        assertEquals(fanera.calculateSquare(),fanera.getHeight()*fanera.getWidth(),0.001);

    }

    @Test
    public void faneraPriceTest()
    {
        Fanera fanera = new Fanera((float)0.3 ,(float)0.4,(float)3.3);
        assertEquals(fanera.calculatePrice(),fanera.getHeight()*fanera.getWidth()*fanera.getPerOnePrice(),0.001);

    }

    ////////////////////////////////////////////////////////
   @Test
    public void framePriceTest()
    {
        Frame frame = new Frame(30,40);
        Baget baget = new Baget((float)0.3, (float)0.4,(float)8.3);
        Fanera fanera = new Fanera((float)0.3, (float)0.4,(float)3.3);
        Glass glass = new Glass((float)0.3 ,(float)0.4,(float)5.4);

        frame.setOrder(true,true,true);
        assertEquals(frame.calculatePrice(), baget.calculatePrice() + fanera.calculatePrice() + glass.calculatePrice() + frame.perOnePriceFrame, 0.001);
    }





}
