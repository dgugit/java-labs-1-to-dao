package JDBC;

import frame_pack.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FramesElementsDao {

    private static Connection connection;

    public FramesElementsDao() {
        connection = ConnectionManager.getConnection();
    }

    public static List<frameElement> getElementsByFrameId(int frameID, float heightFrame, float widthFrame) {
        if (connection == null)
            connection = ConnectionManager.getConnection();

        List<frameElement> elements = new ArrayList<frameElement>();
        String sqlQuery = "select frame_id, type, per_one_price from frame_elem where frame_id =?";

        try (PreparedStatement pStmt = connection.prepareStatement(sqlQuery)) {
            pStmt.setInt(1, frameID);

            ResultSet rez = pStmt.executeQuery();

            while (rez.next()) {
                if (rez.getString("type").replaceAll(" ", "").equals("Baget")) {
                    Baget bt = new Baget();
                    bt.setOnePrice(rez.getFloat("per_one_price"));
                    bt.setHeight(heightFrame);
                    bt.setWidth(widthFrame);
                    elements.add(bt);
                    bt = null;
                }

                if (rez.getString("type").replaceAll(" ", "").equals("Fanera")) {
                    Fanera ft = new Fanera();
                    ft.setOnePrice(rez.getFloat("per_one_price"));
                    ft.setHeight(heightFrame);
                    ft.setWidth(widthFrame);
                    elements.add(ft);
                    ft = null;
                }

                if (rez.getString("type").replaceAll(" ", "").equals("Glass")) {
                    Glass gl = new Glass();
                    gl.setOnePrice(rez.getFloat("per_one_price"));
                    gl.setHeight(heightFrame);
                    gl.setWidth(widthFrame);
                    elements.add(gl);
                    gl = null;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return elements;
    }


}
