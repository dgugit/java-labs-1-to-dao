package JDBC;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {
    private static Connection connection;

    private static Properties getProperties() {
        FileInputStream fis;
        Properties property = new Properties();
        try {
            fis = new FileInputStream("src/main/resources/DBconfiguration.properties");
            property.load(fis);

        } catch (IOException e) {
            System.err.println("Error: DBconfig.properties has an error  ");
        }
        return property;
    }

    private static void makeConnection() {
        Properties properties = getProperties();
        String jdbc_driver = properties.getProperty("jdbc.driver");
        String url = properties.getProperty("jdbc.url");
        try {
            Class.forName(jdbc_driver);
            connection = DriverManager.getConnection(url, properties);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        if (connection == null)
            makeConnection();
        return connection;
    }


}
