package frame_pack;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "baget" )
public class Baget extends frameElement
{


    @Override
    public float calculateSquare()
    {

        return 2*(height + width) ;
    }

    @JsonCreator
    public Baget( @JsonProperty("height")float height, @JsonProperty("width")float width, @JsonProperty("perOnePrice")float perOne)
    {
        super(height, width);
        this.perOnePrice = perOne;
    }

    public Baget(){}

}
