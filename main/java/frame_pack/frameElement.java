package frame_pack;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.sun.xml.internal.ws.developer.Serialization;

import javax.xml.bind.annotation.*;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Baget.class),
        @JsonSubTypes.Type(value = Glass.class),
        @JsonSubTypes.Type(value = Fanera.class),})
@XmlTransient

abstract public class frameElement
{
//    @XmlElements({
//            @XmlElement(name = "height",type=Baget.class),
//            @XmlElement(name = "height",type = Fanera.class),
//            @XmlElement(name = "height",type = Glass.class),
//    })
    protected float height;
    protected float width;
    @XmlElement
    protected  float perOnePrice;

    public frameElement(){}
    public frameElement(float height, float width)
    {
        this.height = height;
        this.width = width;
    }

    public float getHeight() { return height; }
    public float getWidth() { return width; }
    public double getPerOnePrice() { return perOnePrice; }

    public void setHeight(float height) { this.height = height; }
    public void setWidth(float width) { this.width = width; }
    public void setOnePrice(float onePrice) { this.perOnePrice = onePrice; }

     public float calculatePrice()
    {
        return  calculateSquare()  * perOnePrice;
    }
    public float calculateSquare()
    {
        return width*height;
    }




}
